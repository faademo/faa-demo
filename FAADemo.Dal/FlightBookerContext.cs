﻿using System.Data.Entity;
using FAADemo.Dal.Models;
using FAADemo.Dal.Migrations;

namespace FAADemo.Dal
{
    public class FlightBookerContext:DbContext
    {
        public FlightBookerContext() : base("FlightBookerContext")
        {
            Database.SetInitializer<FlightBookerContext>(
                new FlightBookerInitializer<FlightBookerContext, Configuration>());
        }

        public DbSet<AirplaneType> AirplaneTypes { get; set; }
        public DbSet<Airport> Airports { get; set; } 
        public DbSet<Airline> Airlines { get; set; }
        public DbSet<Flight> Flights { get; set; }
        public DbSet<Passenger> Passengers { get; set; }
        public DbSet<PassengerFlight> PassengerFlights { get; set; } 

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Airport>()
                .HasMany(a => a.DepartureFlights)
                .WithRequired(f => f.DepartureAirport)
                .HasForeignKey(f => f.DepartureLocationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Airport>()
                .HasMany(a => a.ArrivalFlights)
                .WithRequired(f => f.ArrivalAirport)
                .HasForeignKey(f => f.ArrivalLocationId)
                .WillCascadeOnDelete(false);
        }
    }
}
