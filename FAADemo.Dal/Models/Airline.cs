﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAADemo.Dal.Models
{
    public class Airline
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string AirlineCode { get; set; }
        [Required]
        public string PhoneNumber { get; set; }

        public virtual ICollection<Flight> Flights { get; set; }
    }
}
