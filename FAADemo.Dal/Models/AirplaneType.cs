﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace FAADemo.Dal.Models
{
    public class AirplaneType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Manufacturer { get; set; }
        [Required]
        public string ModelNumber { get; set; }
        [Required]
        public int TotalRows { get; set; }
        [Required]
        public int SeatsPerRow { get; set; }
        public int FirstClassRows { get; set; }
        public int FirstClassSeatsPerRow { get; set; }

        public virtual ICollection<Flight> Flights { get; set; }
    }
}
