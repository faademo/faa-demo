﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAADemo.Dal.Models
{
    public class Airport
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [MaxLength(3)]
        [Required]
        public string AirportCode { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        [MaxLength(2)]
        public string State { get; set; }
        [Required]
        [MaxLength(5)]
        public string Zipcode { get; set; }
        [Required]
        public string PhoneNumber { get; set; }

        public ICollection<Flight> ArrivalFlights { get; set; }
        public ICollection<Flight> DepartureFlights { get; set; }
    }
}
