﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAADemo.Dal.Models
{
    public class PassengerFlight
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public Guid PassengerId { get; set; }
        [Required]
        public Guid FlightId { get; set; }
        [Required]
        public int RowNumber { get; set; }
        [Required]
        public int SeatNumber { get; set; }
        [Required]
        public double SeatPrice { get; set; }

        public virtual Passenger Passenger { get; set; }
        public virtual Flight Flight { get; set; }
    }
}
