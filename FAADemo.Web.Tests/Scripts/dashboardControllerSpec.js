﻿/// <reference path="../../FAADemo/Scripts/Lib/angular.js" /> 
/// <reference path="../../FAADemo/Scripts/lib/jquery-1.10.2.js" />
/// <reference path="../../FAADemo/Scripts/lib/angular-mocks.js" /> >
/// <reference path="../../FAADemo/Scripts/lib/angular-route.js" />
/// <reference path="../../FAADemo/Scripts/lib/angular-resource.js" />
/// <reference path="../../FAADemo/Scripts/lib/kendo.all.min.js" />
/// <reference path="../../FAADemo/Scripts/lib/kendo.angular.min.js" />
/// <reference path="../../FAADemo/Scripts/App/app.js" />
/// <reference path="../../FAADemo/Scripts/Controllers/dashboardController.js" />
/// <reference path="jasmine.js" /> 




describe('When using dashboardController ', function () {
    //initialize Angular
    beforeEach(module('flightBooker'));
    //parse out the scope for use in our unit tests.
    var scope,
        controller,
        dashboardService;

    var specials = [
        {
            departureDate: "9/18/2015",
            departureAirport: "DFW",
            arrivalAirport: "BOS",
            price: 128.56
        }
    ];

    beforeEach(inject(function($controller, $rootScope) {
        scope = $rootScope.$new();
        dashboardService = {
            GetSpecialFlights: function() {
                return specials;
            }
        }
        controller = $controller('dashboardController', { $scope: scope, dashboardService: dashboardService });
    }));

    it('specials returns 1 flight', function() {
        expect(scope.specials.length).toBe(1);
    });
});