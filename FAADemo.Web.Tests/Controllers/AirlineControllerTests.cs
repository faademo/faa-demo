﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FAADemo.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using FAADemo.Public.ViewModels;
using FAADemo.Svc.Services;
using Moq;

namespace FAADemo.Controllers.Tests
{
    [TestClass()]
    public class AirlineControllerTests
    {
        private Mock<IAirlineService> _airlineService;
        private AirlineController _controller;

        [TestInitialize]
        public void Initialize()
        {
            _airlineService = new Mock<IAirlineService>();
            _controller = new AirlineController(_airlineService.Object);
        }

        [TestMethod()]
        public void method_should_return_all_airlines()
        {
            _airlineService.Setup(a => a.GetAllAirlines()).Returns(new List<AirlineVm>
            {new AirlineVm(), new AirlineVm(), new AirlineVm()});

            var result = _controller.Get();
           
            Assert.AreEqual(3, result.Count);
        }

        [TestMethod()]
        public void method_should_return_specific_airline()
        {
            var airline = new AirlineDetailsVm
            {
                AirlineInfo = new AirlineVm
                {
                    Id = Guid.NewGuid()
                }
            };

            _airlineService.Setup(a => a.GetAirline(It.IsAny<Guid>())).Returns(airline);
            var result = _controller.Get(airline.AirlineInfo.Id);

            Assert.AreEqual(airline,result);
        }

        [TestMethod()]
        public void method_should_return_model_state_errors()
        {
            var airline = new AirlineVm
            {
                Id = Guid.NewGuid()
            };

            _airlineService.Setup(a => a.AddAirline(It.IsAny<AirlineVm>())).Returns(airline.Id);

            _controller.Request = new HttpRequestMessage();
            _controller.Request.SetConfiguration(new HttpConfiguration());

            _controller.ModelState.AddModelError("Name", "Name is require.");
            _controller.ModelState.AddModelError("AirlineCode", "Airline code is required.");
            _controller.ModelState.AddModelError("PhoneNumber", "Phone number is required.");
            var result = _controller.Post(_controller.Request,airline);

            
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
            Assert.IsNotNull(result.Content);
        }
    }
}