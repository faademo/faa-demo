﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAADemo.Dal;
using FAADemo.Dal.Models;
using FAADemo.Public.ViewModels;
using FAADemo.Svc.Services;

namespace FAADemo.Svc.Services
{
    public class AirlineService:IAirlineService
    {
        private readonly IFlightBookerRepository _repository;

        public AirlineService(IFlightBookerRepository repository)
        {
            _repository = repository;
        }

        public List<AirlineVm> GetAllAirlines()
        {
            return AutoMapper.Mapper.Map<List<Airline>, List<AirlineVm>>(_repository.GetAllAirlines());
        }

        public AirlineDetailsVm GetAirline(Guid airlineId)
        {
            var airline = _repository.GetAirline(airlineId);
            return new AirlineDetailsVm
            {
                AirlineInfo = AutoMapper.Mapper.Map<Airline, AirlineVm>(airline),
                Flights = AutoMapper.Mapper.Map<List<Flight>, List<FlightDetailsVm>>(airline.Flights.ToList())
            };
        }

        public Guid AddAirline(AirlineVm airline)
        {
            return _repository.AddAirline(AutoMapper.Mapper.Map<AirlineVm, Airline>(airline));
        }
    }
}
