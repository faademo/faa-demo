﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAADemo.Dal;
using FAADemo.Dal.Models;
using FAADemo.Public.ViewModels;

namespace FAADemo.Svc.Services
{
    public class PassengerService : IPassengerService
    {
        private readonly IFlightBookerRepository _repository;
        public PassengerService(IFlightBookerRepository repository)
        {
            _repository = repository;
        }

        public Guid AddPassenger(PassengerVm passenger)
        {
            return _repository.AddPassenger(AutoMapper.Mapper.Map<PassengerVm,Passenger>(passenger));
        }
    }
}
