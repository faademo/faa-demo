﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAADemo.Public.ViewModels;

namespace FAADemo.Svc.Services
{
    public interface IAirportService
    {
        List<AirportVm> GetAllAirports();
        AirportDetailsVm GetAirport(Guid airportId);
        void AddAirport(AirportVm airport);
    }
}
