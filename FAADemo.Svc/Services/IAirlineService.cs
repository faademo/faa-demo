﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAADemo.Public.ViewModels;

namespace FAADemo.Svc.Services
{
    public interface IAirlineService
    {
        List<AirlineVm> GetAllAirlines();
        AirlineDetailsVm GetAirline(Guid airlineId);
        Guid AddAirline(AirlineVm airline);
    }
}
