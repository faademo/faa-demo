﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAADemo.Public.ViewModels;

namespace FAADemo.Svc.Services
{
    public interface IFlightService
    {
        List<SpecialFlightVm> GetSpecialFlights();
        List<FlightDetailsVm> GetAllFlights();
        FlightDetailsVm GetFlight(Guid flightId);
    }
}
