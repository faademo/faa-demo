﻿using FAADemo.Dal;
using SimpleInjector;
using SimpleInjector.Packaging;

namespace FAADemo.Svc
{
    public class ServiceIocPackage : IPackage
    {
        public void RegisterServices(Container container)
        {
            container.Register<IFlightBookerRepository, FlightBookerRepository>(Lifestyle.Scoped);
            container.Register<FlightBookerContext>(Lifestyle.Scoped);
        }
    }
}
