﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAADemo.Public.ViewModels
{
    public class AirlineDetailsVm
    {
        public AirlineVm AirlineInfo { get; set; }
        public List<FlightDetailsVm> Flights { get; set; } 
    }
}
