﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FAADemo.Public.ViewModels
{
    public class FlightDetailsVm
    {
        public Guid Id { get; set; }
        public string FlightNumber { get; set; }
        public string Airline { get; set; }
        public DateTime Departure { get; set; }
        public string DepartureLocation { get; set; }
        public DateTime Arrival { get; set; }
        public string ArrivalLocation { get; set; }
        public string AirplaneType { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }
    }
}