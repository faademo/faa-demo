﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAADemo.Public.ViewModels
{
    public class AirportDetailsVm
    {
        public AirportVm Airport { get; set; }
        public List<FlightDetailsVm> DepartureFlights { get; set; }
        public List<FlightDetailsVm> ArrivalFlights { get; set; }
    }
}
