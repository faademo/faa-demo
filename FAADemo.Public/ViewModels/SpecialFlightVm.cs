﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAADemo.Public.ViewModels
{
    public class SpecialFlightVm
    {
        public Guid Id { get; set; }
        public string ArrivalAirport { get; set; }
        public string DepartureAirport { get; set; }
        public DateTime DepartureDate { get; set; }
        public double Price { get; set; }
    }
}
