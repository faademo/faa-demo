﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAADemo.Public.ViewModels
{
    public class AirportVm
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Airport name is required.")]
        public string Name { get; set; }
        [MaxLength(3, ErrorMessage = "Airport code should be 3 characters long.")]
        [Required(ErrorMessage = "An airport code is required.")]
        public string AirportCode { get; set; }
        [Required(ErrorMessage = "City is required.")]
        public string City { get; set; }
        [Required(ErrorMessage = "State is required.")]
        [MaxLength(2, ErrorMessage = "Please enter a valid state abbreviation.")]
        public string State { get; set; }
        [Required]
        [MaxLength(5, ErrorMessage = "Zipcode must be 5 digits long.")]
        [MinLength(5, ErrorMessage = "Zipcode must be 5 digits long.")]
        public string Zipcode { get; set; }
        [Required(ErrorMessage = "Phone number is required.")]
        public string PhoneNumber { get; set; }
    }
}
