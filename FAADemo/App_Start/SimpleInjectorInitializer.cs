using System.Web.Http;
using FAADemo.Dal;
using FAADemo.Svc;
using FAADemo.Svc.Services;
using SimpleInjector.Integration.WebApi;

[assembly: WebActivator.PostApplicationStartMethod(typeof(FAADemo.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace FAADemo.App_Start
{
    using System.Reflection;
    using System.Web.Mvc;

    using SimpleInjector;
    using SimpleInjector.Extensions;
    using SimpleInjector.Integration.Web;
    using SimpleInjector.Integration.Web.Mvc;
    
    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            var servicePackage = new ServiceIocPackage();
            servicePackage.RegisterServices(container);
            InitializeContainer(container);
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            // This is an extension method from the integration package.
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.Verify();
            
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
     
        private static void InitializeContainer(Container container)
        {

            // Register your types, for instance using the scoped lifestyle:
            container.Register<IPassengerService, PassengerService>(Lifestyle.Scoped);
            container.Register<IFlightService, FlightService>(Lifestyle.Scoped);
            container.Register<IAirportService, AirportService>(Lifestyle.Scoped);
            container.Register<IAirlineService, AirlineService>(Lifestyle.Scoped);
        }
    }
}