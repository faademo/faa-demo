﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FAADemo.Public.ViewModels;
using FAADemo.Svc.Services;

namespace FAADemo.Controllers
{
    public class PassengerController : ApiController
    {
        private readonly IPassengerService _passengerService;

        public PassengerController(IPassengerService passengerService)
        {
            _passengerService = passengerService;
        }

        public HttpResponseMessage Post(HttpRequestMessage request, PassengerVm model)
        {
            List<string> errors = new List<string>();
            if (ModelState.IsValid)
            {
                try
                {
                    _passengerService.AddPassenger(model);
                    return request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception ex)
                {
                    errors.Add(ex.Message);
                }
            }
            else
            {
                errors.AddRange(PostErrors());
            }

            return request.CreateResponse(HttpStatusCode.BadRequest, errors);

        }

        private IEnumerable<string> PostErrors()
        {
            return ModelState.Values.SelectMany(v => v.Errors.Select(e => e.ErrorMessage));
        }
    }
}