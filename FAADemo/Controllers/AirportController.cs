﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FAADemo.Public.ViewModels;
using FAADemo.Svc.Services;

namespace FAADemo.Controllers
{
    public class AirportController : ApiController
    {
        private readonly IAirportService _airportService;
        public AirportController(IAirportService airportService)
        {
            _airportService = airportService;
        }

        public List<AirportVm> Get()
        {
            return _airportService.GetAllAirports();
        }

        public AirportDetailsVm Get(Guid id)
        {
            return _airportService.GetAirport(id);
        }

        public HttpResponseMessage Post(HttpRequestMessage request, AirportVm airport)
        {
            List<string> errors = new List<string>();
            if (ModelState.IsValid)
            {
                _airportService.AddAirport(airport);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                errors.AddRange(PostErrors());
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, errors);
        }

        private IEnumerable<string> PostErrors()
        {
            return ModelState.Values.SelectMany(v => v.Errors.Select(e => e.ErrorMessage));
        }
    }
}
