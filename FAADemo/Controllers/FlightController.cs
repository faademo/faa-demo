﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Web.Http;
using FAADemo.Public.ViewModels;
using FAADemo.Svc.Services;

namespace FAADemo.Controllers
{
    public class FlightController : ApiController
    {
        private readonly IFlightService _flightService;

        public FlightController(IFlightService flightService)
        {
            _flightService = flightService;
        }
        public List<SpecialFlightVm> Get()
        {
            return _flightService.GetSpecialFlights();
        }

        public FlightDetailsVm Get (Guid id)
        {
            return new FlightDetailsVm();
        }
    }
}

namespace FAADemo.Public.ViewModels
{
}