﻿"use strict";

(function () {

    var airlineService = function ($resource) {
            return $resource("api/Airline", null, {
                GetAllAirlines: {
                    method: 'GET',
                    isArray: true
                },
                SaveAirline: {
                    method: 'POST',
                    airline:'@airline'
                }
            });
        }

    var module = angular.module("flightBooker");
    module.factory("airlineService", ["$resource", airlineService]);
}());