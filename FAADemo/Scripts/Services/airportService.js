﻿"use strict";

(function () {
    angular.module('flightBooker').factory('airportService', [
        '$resource', function($resource) {
            return {
                Common: $resource("api/Airport", null, {
                    GetAllAirports: {
                        method: 'GET',
                        isArray: true
                    },
                    SaveAirport: {
                        method: 'POST',
                        airport: '@airport'
                    }
                }),
                ByFlightsAndAirline: $resource("api/Airport/FlightsAndAirline", null, {
                    GetAllAirports: {
                        method: 'POST',
                        flightsAndAirline: '@flightsAndAirline'
                    }
                }),
            };
        }
    ]);
})();
