﻿"use strict";

(function () {

    angular.module('flightBooker').factory('passengerService', [
           '$resource', function ($resource) {
               return $resource("api/Passenger", null, {
                   GetAllPassengers: {
                       method: 'GET',
                       isArray: true
                   },
                   save: {
                       method: 'POST',
                       passenger: '@passenger'
                   }
               });
           }
    ]);
}());