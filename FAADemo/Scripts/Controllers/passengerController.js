﻿"use strict";

(function () {
    var module = angular.module("flightBooker");

    var passengerController = function($scope, $location, passengerService) {
        $scope.errors = {}
        $scope.save = function(passenger) {
            passengerService.save(passenger).$promise.then(
                function() { $location.url("/FlightBooker/Index") },
                function(response) { $scope.errors = response.data; }
            );
        };

        $scope.passengerDataSource = [
            {
                id: "Male",
                name: "Male"
            },
            {
                id: "Female",
                name: "Female"
            }
        ];
    }

    module.controller("passengerController", ["$scope","$location", "passengerService","airportService", passengerController]);
}())