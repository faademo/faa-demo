﻿"use strict";

(function () {
    angular.module('flightBooker').controller('airlineController', [
        '$scope', 'airlineService', function($scope, airlineService) {

            $scope.airlineGridOptions = {
                columns: [
                    { 'field': 'airlineCode', 'title': 'Airline Code' },
                    { 'field': 'name', 'title': 'Name' }
                ],
                dataSource: {
                    transport: {
                        read: function(options) {
                            airlineService.GetAllAirlines(function(data) {
                                options.success(data);
                            });
                        }

                    },
                    pageSize: 5
                },
                groupable: true,
                scrollable: false,
                sortable: true,
                pageable: true,
                selectable: 'row'
            };

            $scope.selectionChange = function (dataItem) {
                $scope.selectedItem = dataItem;
                $scope.originalData = angular.copy($scope.selectedItem);
                $scope.airlineValuesAreEqual = angular.equals($scope.selectedItem, $scope.originalData);
                $scope.editBtnText = "Edit";
                $scope.view = {
                    url: "../Templates/Airline/selectedAirline.html"
                }
            };

            $scope.editSelectedAirline = function (action) {
                if (action === "Edit") {
                    $scope.view = {
                        url: "../Templates/Airline/edit.html"
                    }
                    $scope.editBtnText = "Cancel";
                } else {
                    $scope.editBtnText = "Edit";
                    $scope.view = {
                        url: "../Templates/Airline/selectedAirline.html"
                    }
                }
            }

            $scope.save = function(airline) {
                airlineService.SaveAirline(airline).$promise.then(function() {
                    $scope.errors = "";
                    $scope.airlineSave = airline;
                }, function(response) {
                    $scope.errors = response.data;
                });
            }

            $scope.updateAirline = function (selectedItem) {
                //TODO: Add code here to handle the update by calling to airlineService
                $scope.airlineValuesAreEqual = angular.equals($scope.selectedItem, $scope.originalData);
                $scope.editBtnText = "Edit";
                $scope.view = {
                    url: "../Templates/Airline/selectedAirline.html"
                }
            }
        }
    ]);
})();